open Move

(*Returns the position of the first character different from ' '
 * if it exists or -1 if not*) 
let find_not_space s =
  let i = ref 0 and n = String.length s in 
  while !i<n && s.[!i] == ' ' do 
    i := !i + 1
  done;
  if !i < n then
    !i
  else
    -1
       



(* Pour que le décalage soit toujours le même, on ajoute ou non une ligne 
 * d'eau afin de rétablir le décalage souhaité.*)

let [@warning "-8"] parse_map map_file =
  let in_chan = open_in map_file in 
  let lines = ref [] in
  let max_length = ref 0 in
  let max_length_num = ref 0 in 
  let nb_lines = ref 0 in

  (* on veut savoir si la première ligne est décalée ou bien si c'est la seconde
   * Si first_line_shift vaut true, c'est que la première ligne 
   * est décalée vers la droite par rapport à la seconde.
   *)
  let first_line_shift = ref false in
  let first_line_shift_found = ref false in

  (*ici, on commence par déterminer la longueur de la plus grande ligne*)
  begin 

    try 
      while true do

        let line = input_line in_chan in

        if not !first_line_shift_found then 
          begin
            let fst_not_space = find_not_space line in
            if fst_not_space >= 0 then
              (first_line_shift := ((!nb_lines + fst_not_space) mod 2) = 1;
               first_line_shift_found := true);
          end;
        nb_lines := !nb_lines +1 ;

        (* no else because we must enter the condition
         * even if first_line_shift_found was changed to true just before*)
        if !first_line_shift_found then
          begin
            if String.length line > !max_length then 
              (max_length_num := !nb_lines;
               max_length := String.length line);

            lines := line::!lines;

          end
      done
    with
      End_of_file -> () 
  end;

  nb_lines := !nb_lines + if !first_line_shift then 0 else -1;
  max_length_num := !max_length_num +  if !first_line_shift then 1 else 0;


  (* line_length : si la ligne considérée est décalée (ie son modulo 2 est 0) ,
   * !max_length est pair et on tombe sur ce qu'on veut 
   * Sinon, on doit ajouter 1. D'où  !max_length_num mod 2. µ *)
  let line_length = ((!max_length_num mod 2) + !max_length)/2  in
  let map = Array.make_matrix !nb_lines line_length WATER in
  (*there are at most 26 penguins, one per alphabet letter*)
  let penguins_pos = Array.make 26 [] in
  let teleport_pos = Array.make 26 None in 

  (* ici nb_lines sera le numéro de la ligne en cours de traitement dans la 
   * grille on va ici écrire la grille à partir de la fin. *)
  nb_lines := !nb_lines - 1;


  while !lines <> [] do 
    let  line::q = !lines in
    let pos_line = ref (1 - !nb_lines mod 2)  in 
    let c = ref 0 in (* c is the number of the current column*)

    while !pos_line < String.length line do
      begin
        match line.[!pos_line] with
        |' ' -> ()
	|'#' -> map.(!nb_lines).(!c) <- NOSTOP
        |peng when peng >= 'a' && peng <='z' ->
	  let num_peng = int_of_char peng - int_of_char 'a' in
	  map.(!nb_lines).(!c) <- PENGUIN;
          penguins_pos.(num_peng) <- (!nb_lines,!c)::penguins_pos.(num_peng)
        |teleport when teleport >= 'A' && teleport <='Z' ->
	  let num_tel = int_of_char teleport - int_of_char 'A' in
	  begin
	    match teleport_pos.(num_tel) with
	    (*we will create the teleport cell later*)
	    |None -> teleport_pos.(num_tel) <- Some(!nb_lines,!c)
	    |Some(pos) -> map.(!nb_lines).(!c) <- TELEPORT (pos, false);
			  map.(fst pos).(snd pos) <- TELEPORT ((!nb_lines,!c), false)
	  end
        |ice ->
	  let n = int_of_char ice in
	  if n >= 48 && n <= 57 then
            map.(!nb_lines).(!c) <- ICE (n-48)
          else
            map.(!nb_lines).(!c) <- ICE 1
      end;      
      c := !c + 1;
      pos_line := !pos_line + 2

    done;

    lines := q;
    nb_lines := !nb_lines - 1
  done;
  
  (map, penguins_pos)


(*Returns the list of characters used for a player*)
let char_players map_name = 	       
  let (_,peng_pos) = parse_map map_name in
  let char_list = ref [] in
  let not_void_list i l  = match l with
    |[] -> ()
    |_ -> char_list := (char_of_int (int_of_char 'a' + i)):: !char_list
  in
  Array.iteri not_void_list peng_pos;
  List.rev !char_list
   
(*Returns the number of players*)
let nb_player map_name = 	       
  let (_,peng_pos) = parse_map map_name in
  let nb_pl = ref 0 in 
  let not_void_list l = match l with
    |[] -> ()
    |_ -> nb_pl := !nb_pl + 1
  in
  Array.iter not_void_list peng_pos;
  !nb_pl

(*Returns the list of penguins pos for each player*)
let penguins_pos map_name =
  let (_,peng_pos) = parse_map map_name in
  let peng_pos_list = ref [] in 
  let not_void_list l = match l with
    |[] -> ()
    |pos -> peng_pos_list := pos :: !peng_pos_list
  in
  Array.iter not_void_list peng_pos;
  List.rev !peng_pos_list
